// Aufgaben U1 , WebG
// MatM 2018

// Wort umkehren

function reverse_string_short(user_str) {
    console.log("Input:", user_str);
    user_str = user_str.split("").reverse().join("");
    console.log("Output:", user_str);
    return user_str;
}

//  Schaltjahr

function leapyear(int) {
    // convert user input to int
    int = parseInt(int);
    console.log("Input:", int);
    let leapyear_test = int + " ist kein Schaltjahr!";
    if ( int%400 === 0) {
        leapyear_test = int + " ist ein Schaltjahr!";
        console.log("condition: int%400");
    }
    else if (int%100 === 0) {
        leapyear_test = int + " ist kein Schaltjahr!";
        console.log("condition: int%100");
    }
    else if (int%4 === 0) {
        leapyear_test = int + " ist ein Schaltjahr!";
        console.log("condition: int%4");
    }
    else leapyear_test = int + " ist kein Schaltjahr!";

    console.log("Output (Leapyear):", leapyear_test);
    return leapyear_test;
}

//Panagramm

function panagramm_checker(str) {
    console.log("Input:", str);
    let abc = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
        "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
    // make the string all lowercase and remove whitespace
    str = str.toLowerCase().replace(/\s/g, "");

    // make array out of string
    let splited_str = str.split("");

    // check for duplicates in the array, and remove them
    for (let i = 0; i < splited_str.length; i++) {
        for (let next = i + 1; next < splited_str.length; next++) {
            if (splited_str[i] === splited_str[next]) {
                splited_str.splice(next,1);
            }
        }
    }

    // check if all the letters from abc are in the array
    let abc_counter = 0;
    for (i in splited_str) {
        if (i in abc) {
            abc_counter +=1;
        }
    }
    if (abc_counter === 26) {
        console.log("Output (Panagramm): true");
        return "'" + str + "'" + " ist ein Panagramm!";
    }
    else {
        console.log("Output (Panagramm): false");
        return "'" + str + "'" + " ist kein Panagramm!";
    }


}
