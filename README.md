# U1: Aufgaben zu JavaScript
##### Matthias Ming - matthias.ming@students.ffhs.ch
##### WebG.BSc INF 2018.ZH2.HS18/19

## Aufgaben
Die Aufgaben sind unter folgendem Link: https://moodle.ffhs.ch/mod/assign/view.php?id=2141428

## Lösungen
Die Lösungen sind im file ``u1_aufgaben_zu_javascript.js``. Das file kann mit z.B.
``node.js`` ausgeführt werden.
### Bedienung
Die Seite ``u1_aufgaben.html`` in einem Browser öffnen

### Ausgabe
Die Ausgabe ist wie folgt:
* Aufgabe 1: Das eingegebene Wort wird rückwarts ausgegeben.
* Aufgabe 2: Ob die Eingabe ein Schaltjahr ist oder nicht wird ausgegeben.
* Aufgabe 3: Ob die Eingabe ein Panagramm ist oder nicht wird ausgegeben.



